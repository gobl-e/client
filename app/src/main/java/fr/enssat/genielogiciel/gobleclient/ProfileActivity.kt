package fr.enssat.genielogiciel.gobleclient

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.extensions.authentication
import com.github.kittinunf.fuel.json.responseJson
import com.github.kittinunf.result.Result
import fr.enssat.genielogiciel.gobleclient.data.model.Paiement
import fr.enssat.genielogiciel.gobleclient.data.model.PaiementViewModel
import fr.enssat.genielogiciel.gobleclient.databinding.ActivityProfileBinding
import org.json.JSONObject

class ProfileActivity : AppCompatActivity() {
    private lateinit var binding: ActivityProfileBinding
    private var HistoryURL  = "https://api.move-it.tech/user/paiement"
    private var returnBtn: Button? = null
    private var logoutBtn: Button? = null
    private var preferenceHelper: PreferenceHelper? = null
    private var token: String = ""
    private var adapter: HistoriqueAdapter = HistoriqueAdapter()
    private var model: PaiementViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        preferenceHelper = PreferenceHelper(this)
        token = preferenceHelper!!.getToken() ?: ""

        if (token == "") {
            logout(true)
        }

        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile)
        model = ViewModelProvider(this).get(PaiementViewModel::class.java)

        val itemDecor = DividerItemDecoration(applicationContext, DividerItemDecoration.VERTICAL)
        itemDecor.setDrawable(applicationContext.getResources().getDrawable(R.drawable.divider_grid_list))
        binding.drinkList.addItemDecoration(itemDecor)

        binding.drinkList.adapter = adapter

        model!!.paiements.observe(this, Observer<List<Paiement>> { list ->
            adapter.list = list
        })

        getHistory()

        binding.swipeRefresh.setOnRefreshListener {
            getHistory()
        }

        returnBtn = findViewById<View>(R.id.profile_returnBtn) as Button
        returnBtn!!.setOnClickListener {
            val intent = Intent(this@ProfileActivity, HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            this@ProfileActivity.finish()
        }

        logoutBtn = findViewById<View>(R.id.profile_logoutBtn) as Button
        logoutBtn!!.setOnClickListener {
            logout(false)
        }

    }

    private fun getHistory() {
        try {
            Fuel.get(
                HistoryURL
            ).authentication().bearer(token).responseJson { request, response, result ->
                when (result) {
                    is Result.Failure -> {
                        when (response.statusCode) {
                            401 -> logout(true)
                            else -> Toast.makeText(
                                this@ProfileActivity,
                                "Error.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    is Result.Success -> {
                        val jsonObject = JSONObject(result.get().content)
                        val jsonPaiements = jsonObject.getJSONArray("paiements")
                        val paiements = arrayListOf<Paiement>()

                        for (i in 0 until jsonPaiements.length()) {
                            val item = JSONObject(jsonPaiements.getString(i))

                            val p = Paiement(
                                item.getString("amount").toFloat(),
                                item.getString("bar"),
                                item.getString("boisson"),
                                item.getString("created_at")
                            )

                            paiements.add(p)
                        }
                        updateUI(paiements)
                    }
                }
            }
        } catch (e: Exception) {
            Toast.makeText(
                this@ProfileActivity,
                "Une erreur est survenue",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun updateUI(paiements: List<Paiement>) {
        binding.swipeRefresh.isRefreshing = false
        model!!.clear()
        model!!.addList(paiements)
        adapter.notifyDataSetChanged()
    }

    private fun logout(auto: Boolean) {
        if (auto) {
            Toast.makeText(
                this@ProfileActivity,
                "Session expirée, veuillez vous reconnecter",
                Toast.LENGTH_SHORT
            ).show()
        }
        preferenceHelper!!.putIsLogin(false)
        val intent = Intent(this@ProfileActivity, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        this@ProfileActivity.finish()
    }
}
