package fr.enssat.genielogiciel.gobleclient

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import net.glxn.qrgen.android.QRCode


class ScanActivity : AppCompatActivity() {
    private var mImgPreview: ImageView? = null
    private var preferenceHelper: PreferenceHelper? = null
    private var token: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan)

        preferenceHelper = PreferenceHelper(this)
        token = preferenceHelper!!.getToken() ?: ""

        findViewById<Button>(R.id.btn_scan_cancel).setOnClickListener {
            val intent = Intent(this@ScanActivity, HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            this.finish()
        }
        mImgPreview = findViewById(R.id.scan_imgView_qr)

        val bitmap = QRCode.from(token).withSize(1000, 1000).bitmap()
        (mImgPreview as ImageView).setImageBitmap(bitmap)
    }
}