package fr.enssat.genielogiciel.gobleclient

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import fr.enssat.genielogiciel.gobleclient.data.model.Paiement

class HistoriqueViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
    val montantTextView = view.findViewById(R.id.paiement_montant) as TextView
    val dateTextView = view.findViewById(R.id.paiement_date) as TextView
    val lieuTextView = view.findViewById(R.id.paiement_lieu) as TextView
    val boissonTextView = view.findViewById(R.id.paiement_boisson) as TextView


    fun setPaiement(paiement: Paiement) {
        montantTextView.text = paiement.montant.toString() + "€"
        dateTextView.text = paiement.date
        lieuTextView.text = paiement.bar
        boissonTextView.text = paiement.boisson
    }
}

class HistoriqueAdapter :
    RecyclerView.Adapter<HistoriqueViewHolder>() {

    var list: List<Paiement> = emptyList()
        set(l) {
            field = l
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int {
        return this.list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoriqueViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.paiement_item_list, parent, false)
        return HistoriqueViewHolder(view)
    }

    override fun onBindViewHolder(holder: HistoriqueViewHolder, position: Int) {
        holder.setPaiement(this.list[position])
    }
}