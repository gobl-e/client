package fr.enssat.genielogiciel.gobleclient

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.json.responseJson
import com.github.kittinunf.result.Result
import fr.enssat.genielogiciel.gobleclient.Utils.Companion.getErrorMessage
import fr.enssat.genielogiciel.gobleclient.Utils.Companion.hideKeyboard
import org.json.JSONException
import org.json.JSONObject


class MainActivity : Activity() {
    private var LoginURL = "https://api.move-it.tech/login"
    private var etemail: EditText? = null
    private var etpassword: EditText? = null
    private var btnlogin: Button? = null
    private var preferenceHelper: PreferenceHelper? = null
    private var tvregister: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        preferenceHelper = PreferenceHelper(this)
        tvregister = findViewById<View>(R.id.tvregister) as TextView
        etemail = findViewById<View>(R.id.textEmail) as EditText
        etpassword = findViewById<View>(R.id.textMdp) as EditText
        btnlogin = findViewById<View>(R.id.btn_login) as Button

        if (preferenceHelper!!.getIsLogin()) {
            val intent = Intent(this@MainActivity, HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            this.finish()
        }

        tvregister!!.setOnClickListener {
            val intent = Intent(this@MainActivity, RegisterActivity::class.java)
            startActivity(intent)
        }

        btnlogin!!.setOnClickListener {
            login()
            hideKeyboard()
        }
    }

    private fun login() {
        try {
            Fuel.post(
                LoginURL, listOf(
                    "email" to etemail!!.text.toString()
                    , "password" to etpassword!!.text.toString()
                    , "admin" to 0
                )
            ).responseJson { request, response, result ->
                when (result) {
                    is Result.Failure -> {
                        when (response.statusCode) {
                            422 -> handleError(String(response.data))
                            401 -> Toast.makeText(
                                this@MainActivity,
                                "Unknown login and/or password.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    is Result.Success -> {
                        onTaskCompleted(result.get().content)
                    }
                }
            }
        } catch (e: Exception) {
            Toast.makeText(
                this@MainActivity,
                "Une erreur est survenue",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun onTaskCompleted(response: String) {
        val jsonObject = JSONObject(response)

        if (jsonObject.getString("message") == "LOGGED") {
            preferenceHelper!!.putIsLogin(true)
            try {
                preferenceHelper!!.putToken(jsonObject.getString("token"))
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            val intent = Intent(this@MainActivity, HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            this.finish()
        } else {
            Toast.makeText(this@MainActivity, getErrorMessage(response), Toast.LENGTH_SHORT).show()
        }
    }

    private fun handleError(response: String) {
        val jsonObject = JSONObject(response)
        val keys = jsonObject.keys()

        while (keys.hasNext()) {
            val key = keys.next()
            val message = jsonObject.optJSONArray(key)[0].toString()

            when (key) {
                "email" -> etemail?.error = message
                "password" -> etpassword?.error = message
            }
        }
    }

}
