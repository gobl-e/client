package fr.enssat.genielogiciel.gobleclient.data.model

class Paiement(
    val montant: Float,
    var bar: String,
    var boisson: String,
    var date: String
)