package fr.enssat.genielogiciel.gobleclient

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.extensions.authentication
import com.github.kittinunf.fuel.json.responseJson
import com.github.kittinunf.result.Result
import com.stripe.android.ApiResultCallback
import com.stripe.android.PaymentConfiguration
import com.stripe.android.PaymentIntentResult
import com.stripe.android.Stripe
import com.stripe.android.model.ConfirmPaymentIntentParams
import com.stripe.android.model.StripeIntent
import com.stripe.android.view.CardInputWidget
import fr.enssat.genielogiciel.gobleclient.Utils.Companion.hideKeyboard
import kotlinx.android.synthetic.main.activity_loadaccount.*
import org.json.JSONObject
import java.lang.ref.WeakReference


class LoadAccountActivity : Activity() {
    private var PaymentURL = "https://api.move-it.tech/charge"
    private var preferenceHelper: PreferenceHelper? = null
    private var token: String = ""
    private var btnreturn: Button? = null
    private lateinit var publishableKey: String
    private lateinit var paymentIntentClientSecret: String
    private lateinit var stripe: Stripe

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loadaccount)

        PaymentConfiguration.init(
            applicationContext,
            "pk_test_jiS1Pf15ZzgZOVgSxpRewmOE00NHvLHKCP"
        )

        preferenceHelper = PreferenceHelper(this)
        token = preferenceHelper!!.getToken() ?: ""
        btnreturn = findViewById<View>(R.id.btn_back) as Button

        val votesValue: ArrayList<Button> = arrayListOf(
            findViewById(R.id.button1),
            findViewById(R.id.button2),
            findViewById(R.id.button3)
        )

        for (i in 0 until 3) {
            votesValue[i].setOnClickListener {
                currentAmountTextView.text = votesValue.get(i).text.toString()

                cardInputWidget.visibility = View.VISIBLE
                payButton.visibility = View.VISIBLE
                startCheckout()
            }
        }

        btnreturn!!.setOnClickListener {
            val intent = Intent(this@LoadAccountActivity, HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            this.finish()
        }
    }

    private fun startCheckout() {
        val weakActivity = WeakReference<Activity>(this)

        try {
            Fuel.post(
                PaymentURL, listOf(
                    "amount" to Integer.parseInt(currentAmountTextView.text.toString()) * 100
                )
            ).authentication().bearer(token).responseJson { request, response, result ->
                when (result) {
                    is Result.Failure -> {
                        displayAlert(weakActivity.get(), "Failed to load page", "Error: $response")
                    }
                    is Result.Success -> {
                        val json = JSONObject(result.get().content)

                        publishableKey = json.getString("publishable_key")
                        paymentIntentClientSecret = json.getString("client_secret")
                        stripe = Stripe(applicationContext, publishableKey)
                    }
                }
            }
        } catch (e: Exception) {
            Toast.makeText(
                this@LoadAccountActivity,
                "Une erreur est survenue",
                Toast.LENGTH_SHORT
            ).show()
        }

        // Hook up the pay button to the card widget and stripe instance
        val payButton: Button = findViewById(R.id.payButton)
        payButton.setOnClickListener {
            val cardInputWidget =
                findViewById<CardInputWidget>(R.id.cardInputWidget)
            val params = cardInputWidget.paymentMethodCreateParams
            if (params != null) {
                val confirmParams = ConfirmPaymentIntentParams
                    .createWithPaymentMethodCreateParams(params, paymentIntentClientSecret)
                stripe.confirmPayment(this, confirmParams)
            }
            hideKeyboard()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val weakActivity = WeakReference<Activity>(this)

        // Handle the result of stripe.confirmPayment
        stripe.onPaymentResult(requestCode, data, object : ApiResultCallback<PaymentIntentResult> {
            override fun onSuccess(result: PaymentIntentResult) {
                val paymentIntent = result.intent
                val status = paymentIntent.status
                if (status == StripeIntent.Status.Succeeded) {
                    Toast.makeText(
                        this@LoadAccountActivity,
                        "Succès du paiement.",
                        Toast.LENGTH_SHORT
                    ).show()
                    val intent = Intent(this@LoadAccountActivity, HomeActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                    this@LoadAccountActivity.finish()
                } else if (status == StripeIntent.Status.RequiresPaymentMethod) {
                    displayAlert(
                        weakActivity.get(),
                        "Échec du paiement.",
                        paymentIntent.lastPaymentError?.message ?: ""
                    )
                }
            }

            override fun onError(e: Exception) {
                displayAlert(weakActivity.get(), "Échec du paiement.", e.toString())
            }
        })
    }

    private fun displayAlert(activity: Activity?, title: String, message: String) {
        if (activity == null) {
            return
        }
        runOnUiThread {
            val builder = AlertDialog.Builder(activity)
            builder.setTitle(title)
            builder.setMessage(message)
            builder.setPositiveButton("Ok", null)

            val dialog = builder.create()
            dialog.show()
        }
    }
}
