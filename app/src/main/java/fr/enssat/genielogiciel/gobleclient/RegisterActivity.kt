package fr.enssat.genielogiciel.gobleclient

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.json.responseJson
import com.github.kittinunf.result.Result
import fr.enssat.genielogiciel.gobleclient.Utils.Companion.getErrorMessage
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException


class RegisterActivity : Activity() {
    private var RegisterURL = "https://api.move-it.tech/register"
    private var etFirstName: EditText? = null
    private var etLastName: EditText? = null
    private var etbirthdate: EditText? = null
    private var etemail: EditText? = null
    private var etpassword: EditText? = null
    private var etconfirmPassword: EditText? = null
    private var btnregister: Button? = null
    private var btnreturn: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        etFirstName = findViewById<View>(R.id.textPrenom) as EditText
        etLastName = findViewById<View>(R.id.textNom) as EditText
        etbirthdate = findViewById<View>(R.id.textDdn) as EditText
        etemail = findViewById<View>(R.id.textEmail) as EditText
        etpassword = findViewById<View>(R.id.textMdp) as EditText
        etconfirmPassword = findViewById<View>(R.id.textMdpConfirm) as EditText

        btnregister = findViewById<View>(R.id.btn_register) as Button
        btnreturn = findViewById<View>(R.id.btn_retour) as Button

        btnreturn!!.setOnClickListener {
            onBackPressed()
        }

        btnregister!!.setOnClickListener {
            register()
        }
    }

    @Throws(IOException::class, JSONException::class)
    private fun register() {

        try {
            Fuel.post(
                RegisterURL, listOf(
                    "name" to etFirstName!!.text.toString()
                    , "surname" to etLastName!!.text.toString()
                    , "birthDate" to etbirthdate!!.text.toString()
                    , "email" to etemail!!.text.toString()
                    , "password" to etpassword!!.text.toString()
                    , "password_confirmation" to etconfirmPassword!!.text.toString()
                    , "admin" to 0
                )
            ).responseJson { request, response, result ->
                when (result) {
                    is Result.Failure -> {
                        when (response.statusCode) {
                            409 -> etemail?.error = "Cet e-mail est déjà pris."
                            422 -> handleError(String(response.data))
                        }
                    }
                    is Result.Success -> {
                        onTaskCompleted(result.get().content)
                    }
                }
            }
        } catch (e: Exception) {
            Toast.makeText(
                this@RegisterActivity,
                "Une erreur est survenue",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun onTaskCompleted(response: String) {
        val jsonObject = JSONObject(response)

        if (jsonObject.optString("message") == "CREATED") {
            Toast.makeText(
                this@RegisterActivity,
                "Inscription réussie ! Vous pouvez vous connecter.",
                Toast.LENGTH_SHORT
            ).show()
            val intent = Intent(this@RegisterActivity, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            this.finish()
        } else {
            Toast.makeText(this@RegisterActivity, getErrorMessage(response), Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun handleError(response: String) {
        val jsonObject = JSONObject(response)
        val keys = jsonObject.keys()

        while (keys.hasNext()) {
            val key = keys.next()
            val message = jsonObject.optJSONArray(key)[0].toString()

            when (key) {
                "name" -> etFirstName?.error = message
                "surname" -> etLastName?.error = message
                "birthDate" -> etbirthdate?.error = message
                "email" -> etemail?.error = message
                "password" -> etpassword?.error = message
                "password_confirmation" -> etconfirmPassword?.error = message
            }
        }
    }
}
