package fr.enssat.genielogiciel.gobleclient.data.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


class PaiementViewModel : ViewModel() {

    private val _paiementSet = mutableListOf<Paiement>()
    private val _allpaiements = MutableLiveData<List<Paiement>>()

    val paiements: LiveData<List<Paiement>> get() = _allpaiements

    fun clear() {
        _paiementSet.clear()
        _allpaiements.postValue(_paiementSet.toList())
    }

    fun addList(paiements: List<Paiement>) {
        _paiementSet.addAll(paiements)
        _allpaiements.postValue(_paiementSet.toList())
    }
}