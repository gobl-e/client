package fr.enssat.genielogiciel.gobleclient

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.extensions.authentication
import com.github.kittinunf.fuel.json.responseJson
import com.github.kittinunf.result.Result
import org.json.JSONObject
import java.util.*
import kotlin.concurrent.timerTask


class HomeActivity : Activity() {
    private var ProfileURL = "https://api.move-it.tech/user/profile"
    private var RefreshTokenURL = "https://api.move-it.tech/lost-cup"
    private var RecommandationURL = "https://api.move-it.tech/recommendation"
    private var preferenceHelper: PreferenceHelper? = null
    private var token: String = ""
    private var scanBtn: Button? = null
    private var lostCupBtn: Button? = null
    private var solde: TextView? = null
    private var firstname: TextView? = null
    private var recommandationText: TextView? = null
    private var timer: Timer? = null

    override fun onDestroy() {
        timer!!.cancel()
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        preferenceHelper = PreferenceHelper(this)
        token = preferenceHelper!!.getToken() ?: ""
        scanBtn = findViewById<View>(R.id.btn_scan) as Button
        lostCupBtn = findViewById<View>(R.id.btn_lost) as Button
        solde = findViewById<View>(R.id.solde) as TextView
        firstname = findViewById<View>(R.id.fisrtname) as TextView
        recommandationText = findViewById<View>(R.id.recommandation_text) as TextView

        if (token == "") {
            logout(true)
        }

        getProfile()
        getRecommandation()
        timer = Timer()

        timer!!.scheduleAtFixedRate(timerTask {
            getProfile()
        }, 0, 2000)

        scanBtn!!.setOnClickListener {
            val intent = Intent(this@HomeActivity, ScanActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            this@HomeActivity.finish()
        }

        lostCupBtn!!.setOnClickListener {
            refreshToken()
        }


        solde!!.setOnClickListener {
            val intent = Intent(this@HomeActivity, LoadAccountActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            this@HomeActivity.finish()
        }

        firstname!!.setOnClickListener {
            val intent = Intent(this@HomeActivity, ProfileActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            this@HomeActivity.finish()
        }
    }

    private fun logout(auto: Boolean) {
        if (auto) {
            Toast.makeText(
                this@HomeActivity,
                "Session expirée, veuillez vous reconnecter",
                Toast.LENGTH_SHORT
            ).show()
        }
        preferenceHelper!!.putIsLogin(false)
        val intent = Intent(this@HomeActivity, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        this@HomeActivity.finish()
    }

    private fun getProfile() {
        try {
            Fuel.get(
                ProfileURL
            ).authentication().bearer(token).responseJson { request, response, result ->
                when (result) {
                    is Result.Failure -> {
                        when (response.statusCode) {
                            401 -> logout(true)
                            else -> Toast.makeText(
                                this@HomeActivity,
                                "Erreur pendant le login.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    is Result.Success -> {
                        val jsonObject = JSONObject(result.get().content)
                        solde!!.text = solde!!.getContext()
                            .getString(R.string.balance_amount, jsonObject.getDouble("balance"))
                        firstname!!.text = jsonObject.getString("name")
                    }
                }
            }
        } catch (e: Exception) {
            Toast.makeText(
                this@HomeActivity,
                "Une erreur est survenue",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun refreshToken() {
        try {
            Fuel.get(
                RefreshTokenURL
            ).authentication().bearer(token).responseJson { request, response, result ->
                when (result) {
                    is Result.Failure -> {
                        when (response.statusCode) {
                            401 -> logout(true)
                            else -> Toast.makeText(
                                this@HomeActivity,
                                "Une erreur est survenue",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    is Result.Success -> {
                        Toast.makeText(
                            this@HomeActivity,
                            "Votre gobelet a bien été désactivé.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        } catch (e: Exception) {
            Toast.makeText(
                this@HomeActivity,
                "Une erreur est survenue",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun getRecommandation() {
        try {
            Fuel.get(
                RecommandationURL
            ).authentication().bearer(token).responseJson { request, response, result ->
                when (result) {
                    is Result.Failure -> {
                        when (response.statusCode) {
                            401 -> logout(true)
                            else -> Toast.makeText(
                                this@HomeActivity,
                                "Une erreur est survenue",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    is Result.Success -> {
                        val jsonObject = JSONObject(result.get().content)
                        val boissons = jsonObject.getJSONArray("boissons")
                        if (boissons.length() > 0) {
                            recommandationText!!.text = "Vous pourriez aimer boire : " + boissons[0]
                        }
                    }
                }
            }
        } catch (e: Exception) {
            Toast.makeText(
                this@HomeActivity,
                "Une erreur est survenue",
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}
